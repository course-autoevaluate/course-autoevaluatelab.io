# Admin

The administration of your `COURSE` instance is available at : `http://votreinstance/admin`

## General configuration 

![admin_menu](figures/admin_menu.png)




## API KEY 

To create a secret key click from the main menu on the `add` button as shown in the image above.

![main_menu_api_key](figures/main_menu_api_key.png)

You will arrive on a page with a form. 

The only mandatory field is the name of the secret key. 

![api_key_new](figures/api_key_new.png)

You can also define an expiration date for the key. 

Once the form is validated you will be redirected to a page presenting the secret key, this page will only be displayed once. 


![api_key_create](figures/api_key_create.png)

## User and Groups

To modify the group or the permissions of a user you have to go to the "users" menu of your administration interface. Select a user. 

![user_perm](figures/user_perm.png)

Select the group from the list on the left and add to the list on the right using the arrows. Do the reverse operation to remove the group from the user. 


