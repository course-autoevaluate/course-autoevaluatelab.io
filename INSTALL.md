# Manual installation

## *COURSE* server

The server is written in Python using the Django framework. 
The database management system is POSTGRESQL. 

### Prerequisites

- a `PostgreSQL server 
- an `Nginx server or another HTTP server

### Download the last release

Clone the project :

```
git clone https://gitlab.com/course-autoevaluate/backend.git
```

### Create a virtual environment

From the `COURSE` directory :

```
python -m venv venv 
```

> Here, we assume that the python command points to a python3.8 or higher executable.


```
venv/bin/pip install -r requirements.txt
```

### Database 

First, you have to configure a database and a user for *course*. 

The environment variables to be configured for the database are :

- `DATABASE_HOST` the hostname of your `PostgreSQL server (if not localhost)
- `DATABASE_USER` is  the name of the user dedicated to your database 
- `DATABASE_NAME` the name of the database 
- `DATABASE_PORT` the port of your `PostgreSQL server (if different from *5432*). 

You can for example, follow the instructions in the section "Create the PostgreSQL Database and User" of this [tutorial](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-20-04) to creating a user and configure a `PostgreSQL database for Django. 


### `SECRET_KEY` 

[Django Documentation](https://docs.djangoproject.com/en/3.2/ref/settings/#secret-key)

For security reasons, you must define a `SECRET_KEY`. You must [keep it secret and safe](https://www.youtube.com/watch?v=iThtELZvfPs). 

You can, for example, use this command to generate a secret key. 

```bash
python -c "import secrets; print(secrets.token_urlsafe())"
```

It may be necessary to install the `secrets` module.

```bash
pip install secrets
```

If an error occurs, try to install these dependencies. 

```bash
sudo apt-get install -y python-dev libldap2-dev libsasl2-dev libssl-dev
```

Define the `SECRET_KEY` environment variable :


```bash
SECRET_KEY=<your_secret_key>
```

### Standard account system 

By default, the connection for teacher and student uses the account manager provided by Django. 

If you only want the `GitLab` connection and do not want to use the standard account system 
you can disable it by setting the following variable to FALSE.

```bash
ACTIVATE_DEFAULT_LOGIN=False
```

### Connect with `GitLab`

By default, the connection with `GitLab` is not active. 

If you want to activate the connection with `GitLab`, change the following variable.

```bash
ACTIVATE_GITLAB_LOGIN=True
```

For the connection with `GitLab`, you have to define two environment variables for 
the client id and the secret key.

```bash
SOCIAL_AUTH_GITLAB_KEY=<your client id>
SOCIAL_AUTH_GITLAB_SECRET=<your secret>
```



Finally, if you want to modify the URL to match your `GitLab instance you 
have to change the following variable: 


```bash
SOCIAL_AUTH_GITLAB_API_URL=http://yourinstancegitlab.com
```

### Creation of tables 

We can generate the tables and the essential data.

From the `COURSE` directory :

```bash
python manage.py makemigrations 
python manage.py migrate
```

### Creation of `superuser`

```bash
python manage.py createsuperuser
```

### Create the `teacher` and `student` group 


```bash
python manage.py createbasegroup
```

### Creation of `executables`

```bash
python manage.py createbaseexecutable
```

By default, a comparison executable is created. This executable will be used to compare the stdout of a student's program with the expected stdout. 

This executable is the same as the one used by [domjudge](https://github.com/DOMjudge/domjudge/tree/main/sql/files/defaultdata/compare). 

For the moment, only the administrator can manage the executables. When creating an exercise, the teacher can choose one of the available programs to compare the results. 


### Creation of `language`

```bash
python manage.py createbaselanguage
```


### Run gunicorn 

```bash
gunicorn --config gunicorn-cfg.py --daemon core.wsgi 
```

### Configure `Nginx.`

Create a new Nginx configuration with a  proxy to the gunicorn daemon.

```
server {
    listen      80;

    location / {
        proxy_pass http://localhost:5005/;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

}

```

---

## *COURSE* runner


The runner allows regularly checking if there are new jobs, and if there are, it will launch the jobs sequentially.

It is possible to deploy several runners on different machines or isolated environments (e.g., docker, for example). 


### Installation 

```bash
pip install course-runner
```


### Configure environment variables

For work, it is necessary to configure several environment variables. 

The first `COURSE_URL` allows you to configure the link to your instance of the `COURSE` server. 

The second `COURSE_API_KEY` is used to configure the API key generated from the `COURSE` server administration space. 

```bash
COURSE_URL=http://yourinstance
COURSE_API_KEY=<your_token>
```

Finally, it is possible to configure the waiting time between two job checks (by default, 60 seconds is the default value). 

```bash
TIME_WAITING=5
```

### Service Linux (not tested)

You can configure a service to easily use system commands to start, stop and restart the runner.

```
[Service]
ExecStart=course-runner

Environment="COURSE_URL=http://yourinstance"
Environment="COURSE_API_KEY=token"
Environment="TIME_WAITING=5"
```


