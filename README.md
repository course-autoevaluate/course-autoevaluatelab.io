# cOUrSE - autOevaluate yoUr Student Easily

[![pipeline status](https://gitlab.com/course-autoevaluate/course-autoevaluate.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/course-autoevaluate/course-autoevaluate.gitlab.io/-/commits/master)

---

This project makes it easy to evaluate students automatically. It allows you to create courses, divided into TP, divided into exercises. For each exercise, it is possible to upload one or more test cases. 
These test cases can be marked as "samples" so that they can be seen by the students. 

Each submission will launch jobs on one of the registered runners. After, the student and the teacher can see a detailed report about the executed job. 

---
