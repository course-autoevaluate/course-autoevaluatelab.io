# Summary

* [Introduction](README.md)
* [Installation](INSTALL.md)
* [About the base groups](AUTH.md)
* [Admin Pages](ADMIN.md)
* [Teacher Pages](TEACHER.md)
* [Student Pages](STUDENT.md)
* [About the badges](BADGES.md)
