# Teacher Page


## Create a course 


When you are on the page listing the available courses, click on the `add` button.

![add_course_button](figures/add_course_button.png)

Fill in the form. 

![add_course_form](figures/add_course_form_fill.png)

On the next page, a success message indicates the success of the creation of the course.
 
![add_course_success](figures/add_course_success_message.png)


## Create a TP with form

When you click on the `show` button, you will be redirected to the course page containing the list of TP. 

![list_tp](figures/list_tp.png)

Fill in the form. 

![add_tp](figures/add_tp_form.png)

On the next page, a success message indicates the success of the creation of TP. 

![tp_list_success](figures/tp_list_success/png)


## Create a TP with an archive  

To create a complete tp with an archive, you need to create a hierarchy like this one below. 


```
── exo1
│   ├── data
│   │   ├── sample
│   │   │   ├── sample01.ans
│   │   │   ├── sample01.in
│   │   │   ├── sample02.ans
│   │   │   └── sample02.in
│   │   └── secret
│   │       ├── test01.ans
│   │       ├── test01.in
│   │       ├── test02.ans
│   │       ├── ....
│   ├── index.MD
│   └── problem.YAML
├── exo2
│   ├── data
│   │   ├── sample
│   │   │   ├── sample01.ans
│   │   │   ├── sample01.in
│   │   │   ├── sample02.ans
│   │   │   └── sample02.in
│   │   └── secret
│   │       ├── hand00.ans
│   │       ├── hand00.in
│   │       ├── hand01.ans
│   │       ├── hand01.in
│   │       ├── ....
│   ├── index.md
│   └── problem.yaml
└── tp.yml

```

At the root of the hierarchy, you need to create a `tp.YAML file with the following fields:

```
name: tp2
number: 2
```

- `name`: the name of the TP
- `number`: the number

For each exercise, create a directory with :

- a `data` directory for test case
    - `sample` directory is the directory for the test case marked sample
    - `secret` directory is the directory for secret test case
- `index.md` is the description of the exercise in markdown 
- `problem.yaml` is a simple YAML configuration with one field `number: 1`

`COURSE` use this [pandoc project](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/statement) for generating a statement for your TP. 

Go to the course page with the list of TP. 

![list_tp](figures/list_tp.png)

Click on `add_tp_with_archive` button. 

![add_tp_archive](figures/add_tp_with_archive.png)

Upload your archive. 


## Create an exercise 

On the page of a TP, you will find the related exercises. 

![exercise_list](figures/exercise_list.png)

Click on the `add` button. 

![add_exercise_button](figures/add_exercise_button.png)

Fill in the form. 

![add_exercise_form](figures/add_exercise_form.png)

## Add test cases with an archive

Create two directories, `sample` and `secret.` In each of the directories, create your test_cases as in the example above. 

```
├── sample
│   ├── sample01.ans
│   └── sample01.in
└── secret
    ├── test01.ans
    └── test01.in

```

Move to the page of the exercise where you want to add test_cases. Click on the edit button. 


![exercise_edit_button](figures/exercise_edit_button.png)


Click on the `test_case` link. 

![exercise_edit_menu](figures/exercise_edit_menu.png)


Click on `add_test_case_with_archive`. 

![add_test_case_with_archive_button](figures/add_test_case_with_archive_button.png)


Upload your archive. 


![add_testcase_with_archive.png](figures/add_testcase_with_archive.png)


 The test cases are present on the next page. 

![list_testcase.png](figures/list_testcase.png)







