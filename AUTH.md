
There are two primary groups in `COURSE`. The first one is the *teacher* group, which allows 
you to create courses, TP, and exercises. 
When a *teacher* joins a course (a *teacher* who does not own the course), they have the right 
to add TP, exercises, and to edit TP that they own. 
We plan to add more granular management of roles in a future version. 

The second one is the *student* group, which allows *students*  see the courses, see the TP and exercises and submit solutions. He can consult only the sample test cases. 


